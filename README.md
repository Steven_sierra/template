MOONBLOCK TEMPLATE
=============
Clone this repository
---------------------
git clone https://bitbucket.org/Steven_sierra/template/src/master/

Install dependencies
--------------------

cd into folder

sudo yarn install

Run and enjoy
-------------
sudo yarn start
  