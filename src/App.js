import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div id='app'>
        <img src={logo} id='logo' alt="logo" />
        <main>
          <div id='container'>
              <div id='images'>
                  <div id='titan'>TITAN</div>
                  <div id='io'>IO</div>
                  <div id='sao'>SAO</div>
                  <div id='phobos'>PHOBOS</div>
                  <div id='ganymede'>GANYMEDE</div>
              </div>
          </div>
          <div id='post-1'> 
              <p>POST 1</p>
              <span>
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                  It has survived not only five centuries, but also the leap into electronic typesetting, 
                  remaining essentially unchanged. It was popularised in the 1960s with the release of 
                  Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
                  software like Aldus PageMaker including versions of Lorem Ipsum.
              </span>
          </div>
          <div id='post-2'> 
              <p>POST 2</p>
              <span>
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                  It has survived not only five centuries, but also the leap into electronic typesetting, 
                  remaining essentially unchanged. It was popularised in the 1960s with the release of 
                  Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
                  software like Aldus PageMaker including versions of Lorem Ipsum.
              </span>
          </div>
          <div id='post-3'>
              <p>POST 3</p>
              <span>
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                  It has survived not only five centuries, but also the leap into electronic typesetting, 
                  remaining essentially unchanged. It was popularised in the 1960s with the release of 
                  Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
                  software like Aldus PageMaker including versions of Lorem Ipsum.
              </span>
          </div>
          <div id='post-4'>
              <p>POST 4</p>
              <span>
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                  It has survived not only five centuries, but also the leap into electronic typesetting, 
                  remaining essentially unchanged. It was popularised in the 1960s with the release of 
                  Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
                  software like Aldus PageMaker including versions of Lorem Ipsum.
              </span>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
